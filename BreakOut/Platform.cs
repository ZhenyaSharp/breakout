﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakOut
{
    class Platform
    {
        public enum MoveDirection
        {
            Left,
            Right
        }

        private int x;
        private int y;
        private int w;
        private int h;

        private int dx;

        private Color color;

        public Platform(int x, int y, int w, int h, int dx, Color color)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.dx = dx;
            this.color = color;
        }

        public void Move(int minX, int maxX, MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.Left:
                    x -= dx;

                    if (x < minX)
                    {
                        x = minX;
                    }
                    break;

                case MoveDirection.Right:
                    x += dx;

                    if (x + w > maxX)
                    {
                        x = maxX - w;
                    }
                    break;
            }
        }

        public void Draw(Graphics graphics)
        {
            graphics.FillRectangle(new SolidBrush(color), x, y, w, h);
        }

        public int X => x;

        public int Y => y;

        public int W => w;

    }
}
