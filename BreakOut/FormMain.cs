﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BreakOut
{
    public partial class Form1 : Form
    {
        private Game game;

        private SoundPlayer soundPlayer;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            game = new Game(pictureBox.Width, pictureBox.Height);

            soundPlayer=new SoundPlayer("Fail.wav");

            pictureBox.Image = game.GetBitmap();

            game.ArrangeBlocks();

            game.Draw();

            timer.Start();

            pictureBox.Refresh();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            game.MoveBall();

            game.ReboundFromBlocks();

            game.ReboundFromPlatform();

            game.ReboundFromBorders();

            game.Draw();

            pictureBox.Refresh();

            if (game.CheckToLose(timer))
            {

                soundPlayer.Play();

                DialogResult result = MessageBox.Show("You Lose! Try Again?", "Fail",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);


                if (result == DialogResult.Yes)
                {
                    Form1_Load(null,null);
                    timer_Tick(null,null);
                }
                if (result == DialogResult.No)
                {
                   Application.Exit();
                }
            }

            if (game.CheckToWin(timer))
            {
                soundPlayer.Play();

                DialogResult result = MessageBox.Show("You Win! Try Again?", "WIN",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    Form1_Load(null, null);
                    timer_Tick(null, null);
                }
                if (result == DialogResult.No)
                {
                    Application.Exit();
                }
            }
        }

        private void FormMain_KeyDown(object sender, KeyEventArgs e)
        {
            game.MovePLatform(e.KeyData);
        }

    }
}
