﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakOut
{
    class Block
    {
        private int x;
        private int y;
        private int w;
        private int h;

        private Color color;

        public Block(int x, int y, int w, int h, Color color)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.color = color;
        }

        public void Draw(Graphics graphics)
        {
            graphics.FillRectangle(new SolidBrush(color), x, y, w, h);
        }

        public int X => x;

        public int Y => y;

        public int W => w;

        public int H => h;
    }
}
