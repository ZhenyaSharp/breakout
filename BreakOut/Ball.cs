﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakOut
{
    class Ball
    {
        private int x;
        private int y;
        private int d;

        private int dx;
        private int dy;

        private Color color;

        private SoundPlayer soundPlayer;

        public Ball(int x, int y, int d, int dx, int dy, Color color)
        {
            this.x = x;
            this.y = y;
            this.d = d;
            this.dx = dx;
            this.dy = dy;
            this.color = color;

            soundPlayer=new SoundPlayer("blocks.wav");
        }

        public void Move()
        {
            x += dx;
            y += dy;
        }

        public void Draw(Graphics graphics)
        {
            graphics.FillEllipse(new SolidBrush(color), x, y, d, d);
        }

        public void CollisionWithBoarders(int minX, int minY, int maxX, int maxY)
        {
            if (y + d >= maxY)
            {
                dy = -dy;
            }

            if (x + d >= maxX)
            {
                dx = -dx;
            }

            if (x <= minX)
            {
                dx = -dx;
            }

            if (y <= minY)
            {
                dy = -dy;
                y = minY;
            }
        }

        public void CollisionWithPlatform(Platform platform)
        {
            if (y + d >= platform.Y && x + d / 2 >= platform.X && x + d / 2 <= platform.X + platform.W)
            {
                soundPlayer.Play();
                dy = -dy;
                y = platform.Y - d - 1;
            }

            
        }

        public void CollisionWithBlocks(List<Block>blocks)
        {
            foreach (var block in blocks)
            {
                if (y <= block.Y + block.H && x >= block.X && x <= block.X + block.W)
                {
                    soundPlayer.Play();
                    blocks.Remove(block);
                    dy = -dy;
                    dx = -dx;
                    return;
                }
            }
        }

        public int Y => y;
        public int D => d;

    }
}
