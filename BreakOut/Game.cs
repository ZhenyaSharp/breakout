﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakOut
{
    class Game
    {
        private Ball ball;

        private Platform platform;

        private List<Block> blocks;

        private Bitmap bitmap;
        private Graphics graphics;

        private int fieldWidth;
        private int fieldHeight;

        private int diagonal;

        public Game(int fieldWidth, int fieldHeight)
        {
            diagonal=(int)Math.Sqrt(Math.Pow(fieldHeight, 2) + Math.Pow(fieldWidth, 2));

            platform = new Platform(fieldHeight / 2 - 40, fieldHeight - 50, diagonal/8, diagonal/74, 12, Color.Red);

            ball = new Ball(platform.X, platform.Y - 30, diagonal/37, 8, 8, Color.Violet);

            blocks = new List<Block>();

            bitmap = new Bitmap(fieldWidth, fieldHeight);
            graphics = Graphics.FromImage(bitmap);

            this.fieldWidth = fieldWidth;
            this.fieldHeight = fieldHeight;


        }

        public Bitmap GetBitmap()
        {
            return bitmap;
        }

        public void Draw()
        {
            graphics.Clear(Color.Gray);

            foreach (var block in blocks)
            {
                block.Draw(graphics);
            }

            platform.Draw(graphics);

            ball.Draw(graphics);
        }

        public void MoveBall()
        {
            ball.Move();
        }

        public void ReboundFromBorders()
        {
            ball.CollisionWithBoarders(0, 0, fieldWidth, fieldHeight);
        }

        public void MovePLatform(Keys key)
        {
            switch (key)
            {
                case Keys.A:
                    platform.Move(0, fieldWidth, Platform.MoveDirection.Left);
                    break;
                case Keys.D:
                    platform.Move(0, fieldWidth, Platform.MoveDirection.Right);
                    break;
            }
        }

        public void ReboundFromPlatform()
        {
            ball.CollisionWithPlatform(platform);
        }

        public void ReboundFromBlocks()
        {
            ball.CollisionWithBlocks(blocks);
        }

        public void ArrangeBlocks()
        {
            int newY = 1;

            for (int j = 0; j < diagonal / 147; j++)
            {
                for (int i = 0; i < diagonal / 122; i++) //krivo
                {
                    blocks.Add(new Block((diagonal / 9 * i) + i, newY, diagonal / 10, diagonal / 50, Color.Blue));
                }

                newY += blocks[j].H + 5;
            }

            /*int newY = 1;

            for (int j = 0; j < 5; j++)
            {
                for (int i = 0; i < 6; i++)
                {
                    blocks.Add(new Block((80 * i) + i, newY, 80, 15, Color.Blue));
                }

                newY += blocks[j].H + 1;
            }*/
        }

        public bool CheckToLose(Timer timer)
        {
            if (ball.Y + ball.D >= fieldHeight)
            {
                timer.Stop();
                return true;
            }

            return false;
        }

        public bool CheckToWin(Timer timer
        )
        {
            if (blocks.Count == 0)
            {
                timer.Stop();
                return true;
            }

            return false;
        }

    }
}
